//
//  UserPagesVC.swift
//  Pitchero-Test
//
//  Created by Russell Warwick on 11/6/18.
//  Copyright © 2018 Russell Warwick. All rights reserved.
//

import UIKit

class UserPagesVC: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet var tableView : UITableView!
    
    //MARK: -
    
    let facebookAPI = FacebookAPIClient()
    var pages : [PageData] = []
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        navigationItem.rightBarButtonItem  = UIBarButtonItem(title: "Logout", style: .done, target: self, action: #selector(logout))
        navigationItem.setHidesBackButton(true, animated:true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchPages()
    }
    
    private func fetchPages(){
        facebookAPI.delegate = self
        facebookAPI.retrieveUsersGroups()
    }
    
    //MARK: - Configure View
    
    private func configureTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(PageTableViewCell.nib, forCellReuseIdentifier: PageTableViewCell.identifier)
        tableView.reloadData()
    }

    //MARK: - Actions
    
    @objc func logout(){
        FBSDKLoginManager().logOut()
        navigationController?.popViewController(animated: true)
    }
    
}

extension UserPagesVC : FacebookAPIClientDelegate {

    //MARK: - FacebookAPIClientDelegate

    func pagesFromLocal(pages: [PageData]) {
        self.pages = pages
        self.tableView.reloadData()
        showAlertView(self, message: "Retrived from local")
    }

    func pagesFromNetwork(pages: [PageData]) {
        self.pages = pages
        self.tableView.reloadData()
        showAlertView(self, message: "Updated from network")
    }

    func errorDidOccur(error: ApplicationError) {
        showAlertView(self, message: error.localizedDescription)
    }

}

extension UserPagesVC : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PageTableViewCell.identifier, for: indexPath) as? PageTableViewCell else { fatalError("Failed to use PageTableViewCell") }
        cell.configure(withVM: PageVM(pageData: pages[indexPath.row]))
        return cell
    }

}
