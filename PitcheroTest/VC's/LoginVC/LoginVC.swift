//
//  ViewController.swift
//  Pitchero-Test
//
//  Created by Russell Warwick on 11/6/18.
//  Copyright © 2018 Russell Warwick. All rights reserved.
//

import UIKit


class LoginVC: UIViewController {

    //MARK: - View Life Cycle
    
    let loginButton : FBSDKLoginButton = {
        let button = FBSDKLoginButton()
        button.readPermissions = ["pages_manage_cta"]
        return button
    }()
    
    override func viewDidLoad() {
        loginButton.delegate = self
        view.addSubview(loginButton)
        loginButton.center = view.center
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkAccessToken()
    }
    
    private func checkAccessToken(){
        if let _ = FBSDKAccessToken.current() {
            let userPagesVC = UserPagesVC(nibName: UserPagesVC.identifier, bundle: nil)
            self.navigationController?.show(userPagesVC, sender: self)
        }
    }
}

extension LoginVC : FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        print("logged in")
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logged out")
    }
}
