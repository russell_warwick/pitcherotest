//
//  AppDelegate.swift
//  PitcheroTest
//
//  Created by Russell Warwick on 11/8/18.
//  Copyright © 2018 Russell Warwick. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        let loginVC = UINavigationController(rootViewController: LoginVC(nibName: LoginVC.identifier, bundle: nil))
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = loginVC
        self.window?.makeKeyAndVisible()
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }


}

